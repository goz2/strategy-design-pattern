### what is it



意图：**策略模式**是一种行为设计模式， 它能让你定义一系列算法， 并将每种算法分别放入独立的类中， 以使算法的对象能够相互替换。

> [策略设计模式 (refactoringguru.cn)](https://refactoringguru.cn/design-patterns/strategy)

（将算法封装在类中，使它们在运行时可重用和互换。）



<img src="https://imagebed233.oss-cn-guangzhou.aliyuncs.com/img/image-20230715170827620.png" alt="image-20230715170827620" style="zoom:50%;" />

- **What:** *A protocol that defines the action that we want to encapsulate. In our example, the action would be* ***log a message\****.*
- **Who:** *An object who contains an object who conforms the strategy. In our example, it could be an object who* ***using the strategy log the message\****.*
- **How:** *Specific implementations of the strategy. Each implementation is different. In our example, we would have* ***three\*** ***strategies,\*** ***one for each style.\***



使用策略模式的原则

1. **Single responsibility.** When we create strategies to do different things, we get that our original class has fewer responsibilities.
2. **Open / Close.** If we use strategies to extend the functionality of the objects we don’t need to update the original object. As in our first example, if would add a new style, we only need to create a new strategy. Otherwise we would need to add a new case in our Styles enum.

> from：[Strategy pattern in Swift | by Juanpe Catalán | Medium](https://medium.com/@JuanpeCatalan/strategy-pattern-in-swift-1462dbddd9fe)
>
> 这个链接中还有很多策略模式的例子



#### 实现方式

1. 从上下文类中找出修改频率较高的算法 （也可能是用于在运行时选择某个算法变体的复杂条件运算符）。
2. 声明该算法所有变体的通用策略接口。
3. 将算法逐一抽取到各自的类中， 它们都必须实现策略接口。
4. 在上下文类中添加一个成员变量用于保存对于策略对象的引用。 然后提供设置器以修改该成员变量。 上下文仅可通过策略接口同策略对象进行交互， 如有需要还可定义一个接口来让策略访问其数据。
5. 客户端必须将上下文类与相应策略进行关联， 使上下文可以预期的方式完成其主要工作。

> from：[策略设计模式 (refactoringguru.cn)](https://refactoringguru.cn/design-patterns/strategy)

![fromGURU](https://imagebed233.oss-cn-guangzhou.aliyuncs.com/img/fromGURU.jpg)

#### 优缺点

**优点**

1. 你可以在运行时切换对象内的算法。
2.  你可以将算法的实现和使用算法的代码隔离开来。
3.  你可以使用组合来代替继承。
4.  *开闭原则*。 你无需对上下文进行修改就能够引入新的策略。

**缺点**

1. 如果你的算法极少发生改变， 那么没有任何理由引入新的类和接口。 使用该模式只会让程序过于复杂。
2.  客户端必须知晓策略间的不同——它需要选择合适的策略。
3.  许多现代编程语言支持函数类型功能， 允许你在一组匿名函数中实现不同版本的算法。 这样， 你使用这些函数的方式就和使用策略对象时完全相同， 无需借助额外的类和接口来保持代码简洁。

> from：[策略设计模式 (refactoringguru.cn)](https://refactoringguru.cn/design-patterns/strategy)



### what kind of problems does it solve

1. 封装算法变化：当系统中某个行为具有多种变体时，策略模式可以将每个变体都封装成一个独立的策略类。这样，客户端就无需知道具体的算法细节，只需要选择合适的策略进行使用。
2. 避免条件语句的过度复杂化：使用策略模式可以避免使用大量的条件语句来实现不同的算法。通过将算法封装成策略类，并在客户端动态选择合适的策略，可以简化代码结构，使得代码更易于理解和维护。
3. 提供可扩展性：策略模式可以让新增的算法变体更加容易地加入到系统中。由于每个算法都被封装在单独的策略类中，新增的算法只需要新增一个策略类，并在客户端进行配置即可。

> **from:chatGPT**





### when do we need to use it

1. 多种算法或行为的选择：当我们有多个类似的算法或行为，但需要在运行时根据条件或配置选择其中之一时，可以使用策略模式。这允许我们根据需要动态地切换算法，而不需要更改客户端代码。
2. 避免条件语句的多重嵌套：如果我们有许多条件语句来选择不同的行为，这可能导致代码的复杂性和难以维护性。使用策略模式，我们可以将每个行为封装在一个独立的策略类中，从而避免条件语句的多重嵌套。
3. 提供灵活性和可扩展性：策略模式使得我们可以轻松地增加、替换或修改现有的算法，而不会影响到使用该算法的客户端代码。这使得系统更灵活，并且易于扩展和维护。

> **from:chatGPT**



1.  当你想使用对象中各种不同的算法变体， 并希望能在运行时切换算法时， 可使用策略模式。

   ​	策略模式让你能够将对象关联至可以不同方式执行特定子任务的不同子对象， 从而以间接方式在运行时更改对象行为。

   

2.  当你有许多仅在执行某些行为时略有不同的相似类时， 可使用策略模式。

    	策略模式让你能将不同行为抽取到一个独立类层次结构中， 并将原始类组合成同一个， 从而减少重复代码。

   

3.  如果算法在上下文的逻辑中不是特别重要， 使用该模式能将类的业务逻辑与其算法实现细节隔离开来。

   ​	策略模式让你能将各种算法的代码、 内部数据和依赖关系与其他代码隔离开来。 不同客户端可通过一个简单接口执行算法， 并能在运行时进行切换。

   

4.  当类中使用了复杂条件运算符以在同一算法的不同变体中切换时， 可使用该模式。

   ​	策略模式将所有继承自同样接口的算法抽取到独立类中， 因此不再需要条件语句。 原始对象并不实现所有算法的变体， 而是将执行工作委派给其中的一个独立算法对象。

> from：[策略设计模式 (refactoringguru.cn)](https://refactoringguru.cn/design-patterns/strategy)



##### Different ways to do the same

When you need to do the same thing in your code by different ways, it’s a clear example that you could use it.

#### Instead of inheritance

If you need to extend the functionality of a class and for doing this, you need to create a new class who inherits from it.

#### Alternative to if/else blocks

This is my favourite. Sometimes, if you look at a class, you can see that it has too much if/else or switch blocks, I mean, conditional blocks. This is a sign that this class has more responsibilities than it should. Using a Strategy pattern would help you distribute them.

> from:[Medium](https://medium.com/@JuanpeCatalan/strategy-pattern-in-swift-1462dbddd9fe)